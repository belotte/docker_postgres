#!/bin/bash

echo "local   all             postgres                                trust" >> /etc/postgresql/10/main/pg_hba.conf
echo "host all  all    0.0.0.0/0  trust" >> /etc/postgresql/10/main/pg_hba.conf
echo "listen_addresses = '*'" >> /etc/postgresql/10/main/postgresql.conf
service postgresql start > /dev/null
runuser -l postgres -c 'createuser root -s'
#createdb prod

chmod 400 /root/.ssh/*

if [[ ! -d /root/db || ! -d /root/db/.git ]]; then
	cd /root
	rm -rf db
	git clone git@gitlab.com:HelloZack/db.git
	cd db
else
	if [[ "$CLEAR_DB" = "" ]]; then
		cd /root/db
		git checkout master
		git pull
	fi
fi
if [[ ! "$DATE" = "" ]]; then
	COMMIT=`git log --before="$DATE" | head -1 | cut -d' ' -f2`
	echo "Commit: $COMMIT"
fi
if [[ ! "$COMMIT" = "" ]]; then
	git checkout $COMMIT
	echo "Checked out ${COMMIT}"
fi

psql postgres < db.save > /dev/null

psql prod
